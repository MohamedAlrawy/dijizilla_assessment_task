# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import get_object_or_404
from users.models import UserAccount
from users.serializers import (
    UserAccountSerializer, ForgetPasswardSerializer,
    ResetPasswordSerializer, LoginSerializer, UserSerializer
)
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from core.helpers import send_token_by_email, get_email_from_token, get_client_info
from rest_framework.authtoken.models import Token
from django.contrib.auth import login as django_login
from django.contrib.auth.models import User


class UserViewSet(ViewSet):
    """end-point to get user by id and
     user should be authenticated"""

    serializer_class = UserAccountSerializer

    def retrieve_user(self, request, pk):
        user = get_object_or_404(UserAccount, id=pk)
        serializer = self.serializer_class(user)
        serializer.data["user"].pop("password")
        return Response(serializer.data, status=status.HTTP_200_OK)


class RegisterViewSet(ViewSet):
    """end-point to register new user
     and allowed to any anonymous user"""

    serializer_class = UserAccountSerializer
    permission_classes = ()

    def register(self, request):
        user_serializer = UserSerializer(data=request.data)
        user_serializer.is_valid(raise_exception=True)
        mobile = request.data.pop("mobile")
        user = User.objects.create_user(**request.data)
        user_account = UserAccount.objects.create(
            user=user, mobile=mobile)
        serializer = self.serializer_class(user_account)
        serializer.data["user"].pop("password")
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class LoginViewSet(ViewSet):
    """end-point to authenticate user
     and allowed to any anonymous user"""

    serializer_class = LoginSerializer
    permission_classes = ()

    def login(self, request):
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        django_login(request, user)
        token, created = Token.objects.get_or_create(user=user)
        return Response({"token": token.key}, status=status.HTTP_200_OK)


class ForgetPasswordViewSet(ViewSet):
    """end-point to send verification token
     to the user by submitting username
     and allowed to any anonymous user"""

    serializer_class = ForgetPasswardSerializer
    permission_classes = ()

    def forget_password(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = get_object_or_404(
            User,
            username=serializer.data.get("username"))
        send_token_by_email(user.email)
        return Response(
            {"message": "An email will be sent to you."},
            status=status.HTTP_200_OK
        )


class ResetPasswordViewSet(ViewSet):
    """end-point to submit token and
    new password to reset password
     and allowed to any anonymous user"""

    serializer_class = ResetPasswordSerializer
    permission_classes = ()

    def reset_password(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        email = get_email_from_token(
            serializer.data.get("token"))
        user = get_object_or_404(
            User,
            email=email)
        user.set_password(serializer.data.get("password"))
        user.save()
        return Response(
            {"message": "Your password will be reset."},
            status=status.HTTP_200_OK
        )


class UserBrowserInfoViewSet(ViewSet):
    """end-point to get browser info
     user should be authenticated
     there is some info because i don't know
     which info i should return"""

    def browser_info(self, request):
        data = get_client_info(request)
        return Response(data, status=status.HTTP_200_OK)
