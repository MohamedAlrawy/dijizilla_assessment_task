from rest_framework import serializers
from users.models import UserAccount
from django.contrib.auth import authenticate
from rest_framework import exceptions
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name', 'password']
        extra_kwargs = {'password': {'write_only': True}}

    def validate_username(self, username):
        """set username to lower case"""
        return username.lower()

    def validate_email(self, email):
        """set email to lower case"""
        return email.lower()


class UserAccountSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserAccount
        fields = '__all__'
        depth = 2


class ForgetPasswardSerializer(serializers.Serializer):

    username = serializers.CharField(required=True)

    def validate_username(self, username):
        """set username to lower case"""
        return username.lower()


class ResetPasswordSerializer(serializers.Serializer):

    token = serializers.CharField(required=True)
    password = serializers.CharField(required=True)
    confirm_password = serializers.CharField(required=True)

    def validate(self, data):
        """Validate that password and confirm_password match"""
        if data["password"] != data["confirm_password"]:
            raise serializers.ValidationError({
                "password": "Password Mismatch!"
            })
        return data


class LoginSerializer(serializers.Serializer):

    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True)

    def validate(self, data):
        username = data.get("username", "")
        password = data.get("password", "")

        if username and password:
            user = authenticate(username=username, password=password)
            if user:
                if user.is_active:
                    data["user"] = user
                else:
                    msg = "User is deactivated."
                    raise exceptions.ValidationError(msg)
            else:
                msg = "Unable to login with given credentials."
                raise exceptions.ValidationError(msg)
        else:
            msg = "Must provide username and password both."
            raise exceptions.ValidationError(msg)
        return data
