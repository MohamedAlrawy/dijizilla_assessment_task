from django.db import models
from django.contrib.auth.models import User
from core.validators import phone_validator
from django.contrib import admin


class UserAccount(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    mobile = models.TextField(blank=True, null=True,
                              validators=[phone_validator])


admin.site.register(UserAccount)
