from django.urls import path

from users.views import (
    UserViewSet, RegisterViewSet, ResetPasswordViewSet,
    ForgetPasswordViewSet, LoginViewSet, UserBrowserInfoViewSet
)

app_name = 'users'
urlpatterns = [
    path('users/<int:pk>/',
         UserViewSet.as_view({
             'get': 'retrieve_user'
         }),
         name='users'),
    path('users/register/',
         RegisterViewSet.as_view({
             'post': 'register'
         }),
         name='register'),
    path('users/forget-password/',
         ForgetPasswordViewSet.as_view({
             'post': 'forget_password'
         }),
         name='forget_password'),
    path('users/reset-password/',
         ResetPasswordViewSet.as_view({
             'post': 'reset_password'
         }),
         name='reset_password'),
    path('users/login/',
         LoginViewSet.as_view({
             'post': 'login'
         }),
         name='login'),
    path('users/browser-info/',
         UserBrowserInfoViewSet.as_view({
             'get': 'browser_info'
         }),
         name='browser_info')
]
