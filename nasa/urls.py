from django.urls import path

from nasa.views import NASAViewSet

app_name = 'nasa'
urlpatterns = [
    path('nasa/',
         NASAViewSet.as_view({
             'get': 'get_nasa_data'
         }),
         name='nasa')
]
