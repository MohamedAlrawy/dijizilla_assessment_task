# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
import requests
from assessment_task import settings


class NASAViewSet(ViewSet):
    """end-point to call NASA api,
     user should be authenticated."""
    """
    note that google service not working 
    the whole day.
    """

    def get_nasa_data(self, request):
        url = settings.NASA_API_URL
        response = requests.get(url=url)
        return Response(response.json(), status=status.HTTP_200_OK)
