
from django.core.validators import RegexValidator

phone_validator = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number not valid.")
