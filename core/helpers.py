import secrets
from django.conf import settings
from django.core.cache import cache
from django.core.mail import send_mail
from django.template.loader import render_to_string
from assessment_task import settings


def send_token_by_email(email, period=settings.PASSWORD_RESET_TOKEN_PERIOD):
    # create token
    token = secrets.token_urlsafe()

    # send token by email logic
    data = {
        'period': int(period / 60),
        'temp_token': token
    }

    msg_plain = render_to_string(
        'email_templates/request_reset_token.txt', data
    )
    msg_html = render_to_string(
        'email_templates/request_reset_token.html', data
    )
    subject, from_email, to = (
        settings.PASSWORD_RESET_EMAIL_SUBJECT,
        settings.FROM_EMAIL,
        email
    )
    send_mail(
        subject, msg_plain, from_email, [to], html_message=msg_html
    )

    # save (token, email) in cache for period time
    cache.set(token, email, timeout=period)


def get_email_from_token(token):
    # get email from cache by token
    email = cache.get(token)
    if not email:
        return None
    return email


def get_client_info(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    data = {
        'url_scheme': request.META.get('wsgi.url_scheme', None),
        'GNOME_SHELL_SESSION_MODE': request.META.get('GNOME_SHELL_SESSION_MODE', None),
        'USERNAME': request.META.get('USERNAME', None),
        'HTTP_CACHE_CONTROL': request.META.get('HTTP_CACHE_CONTROL', None),
        'CSRF_COOKIE': request.META.get('CSRF_COOKIE', None),
        'HTTP_CONNECTION': request.META.get('HTTP_CONNECTION', None),
        'ip': ip
    }
    return data
